using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileScript : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "devil" && gameObject.tag == "lightAttack")
        {
            other.gameObject.GetComponent<devilScript>().hit(player.GetComponent<fireProjectile>().lightDamage);
        }
        else if(other.gameObject.tag == "angel" && gameObject.tag == "darkAttack")
        {
            other.gameObject.GetComponent<devilScript>().hit(player.GetComponent<fireProjectile>().darkDamage);
        }
        Destroy(gameObject);
    }

}
