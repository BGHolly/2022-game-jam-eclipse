using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class devilScript : MonoBehaviour
{
    public Transform player;
    public GameObject gameManager;
    public float speed = 5f;
    private Rigidbody2D rigidBody;
    private Vector2 movement;
    public int health = 20;
    private float AttackStart = 0f;
    private float AttackCooldown = 2f;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position; 
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rigidBody.rotation = angle; 
        direction.Normalize();
        movement = direction;
        if(health <= 0)
        {
            gameManager.GetComponent<gameManager>().liveEnemies--;
            Destroy(gameObject);
        } 
    }

    void FixedUpdate()
    {
        if(Time.time > AttackStart + AttackCooldown)
        {
            moveCharacter(movement);
            AttackStart = Time.time;
        }
    }
    void moveCharacter(Vector2 direction)
    {
        //rigidBody.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
        rigidBody.AddForce(direction, ForceMode2D.Impulse);
    }

    public void hit(int dmg)
    {
        health -= dmg; 
    }
}
