using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
   public int counter;
   public GameObject player;
   public GameObject devil;
   public GameObject angel;
   public GameObject door;
   public GameObject canvas;
   private float start = 0f;
   private float cooldown = 5f;
   public bool waveCleared = true;
   private int numEnemies;
   private int devils;
   private int angels;
   public int liveEnemies = 0;
   // Start is called before the first frame update
   void Start()
   {
      counter = 1;
   }

   void Update()
   {
         
      if (waveCleared == true)
      {
         liveEnemies = 0;
         waveCleared = false;
         this.GetComponent<abilityMenu>().Selection();





         int randomNumber = Random.Range(1, 10);
         
         float enemySplit = Random.value;
         float tempOne = ( Mathf.Pow( (counter-1) ,3) * randomNumber);
         float tempTwo = ( Mathf.Pow(counter, 2f ));
         //Debug.Log((counter-1)**3);
         //Debug.Log(tempTwo);
         numEnemies = 2 + (int)Mathf.Floor( tempOne / tempTwo );

         
         angels = (int)Mathf.Round(numEnemies * enemySplit);
         enemySplit = 1 - enemySplit;
         devils = (int)Mathf.Round(numEnemies * enemySplit);






         
         counter += 1;
         for (int i = 0; i < angels; i++)
         {
            Instantiate(angel, new Vector2(0, 20), Quaternion.identity);
            liveEnemies ++;
         }
         for (int i = 0; i < devils; i++)
         {
            Instantiate(devil, new Vector2(0, 20), Quaternion.identity);
            liveEnemies ++;
         }
      }
      else if(liveEnemies == 0)
      {
         waveCleared = true;
      }
   
   }
}
      /*public void newScene()
      {
         int randomNumber = Random.Range(1, 4);
         if(randomNumber == 1)
         {
            SceneManager.LoadScene("background2", LoadSceneMode.Additive); 
         }    
         else if(randomNumber == 2)
         {
            SceneManager.LoadScene("background3", LoadSceneMode.Additive); 
         } 
         else if(randomNumber == 3)
         {
            SceneManager.LoadScene("background4", LoadSceneMode.Additive); 
         } 
         else if(randomNumber == 4)
         {
            SceneManager.LoadScene("background5", LoadSceneMode.Additive); 
         } 
         GameObject newDoor = Instantiate(door, new Vector2(14, -1), Quaternion.identity);
         GameObject newCanvas = Instantiate(canvas, new Vector2(-16, -2), Quaternion.identity);
         newDoor.GetComponent<doorScript>().abilitySelector = newCanvas;
         float polarity = player.GetComponent<fireProjectile>().polarity;
         if (polarity > 0.8)
         {
            for (int i = 0; i < 8; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
         }
         else if (polarity > 0.5)
         {
            for (int i = 0; i < 6; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
            for (int i = 0; i < 2; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }
         }
         else if (polarity > 0.2)
         {
            for (int i = 0; i < 5; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
            for (int i = 0; i < 3; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }   
         }
         else if (polarity > -0.2)
         {
            for (int i = 0; i < 4; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
            for (int i = 0; i < 4; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }
         }
         else if (polarity > -0.5)
         {
            for (int i = 0; i < 3; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
            for (int i = 0; i < 5; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }
         }
         else if (polarity > -0.8)
         {
            for (int i = 0; i < 2; i++)
            {
               Instantiate(devil, new Vector2(0, 30), Quaternion.identity);
            }
            for (int i = 0; i < 6; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }
         }
         else if (polarity >= -1)
         {
            for (int i = 0; i < 8; i++)
            {
               Instantiate(angel, new Vector2(0, 30), Quaternion.identity);
            }
         }*/
  