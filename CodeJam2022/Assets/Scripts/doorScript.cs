using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorScript : MonoBehaviour
{
    public GameObject abilitySelector;

    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player")
        {
            abilitySelector.GetComponent<abilityMenu>().newAbility = true;
            Destroy(this);
        }
    }
}
