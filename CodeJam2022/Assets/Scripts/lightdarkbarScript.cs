using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightdarkbarScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float distanceCenterToEdge = 1.301F;
    float baseX = 8.04F;
    float baseY = 3.964F;

    // Update is called once per frame
    void Update()
    {
        
    }

    void ChangePolarity(float delta) {
        baseX = transform.position.x;
        baseY = transform.position.y;

        transform.position += new Vector3 (baseX + (delta * distanceCenterToEdge), baseY, 0);
    }
}
