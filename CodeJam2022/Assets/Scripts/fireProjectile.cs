using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireProjectile : MonoBehaviour
{
    public Transform firePos;
    public GameObject lightProjectile;
    public GameObject darkProjectile;
    private Vector2 lookDirection;
    private float lookAngle;
    private float ZapStart = 0f;
    private float ZapCooldown = 1f;
    public int Health = 10;
    public int lightDamage = 10;
    public int darkDamage = 10;
    public int critRate = 5;
    public int critDamage = 50;
    public float polarity = 0;
    public int blight = 0;
    public int corruption = 0;
    public int fearOfGod = 0;
    public int divineProtection = 0;
    public int atonement = 0;
    public int oneMoreInTheChamber = 0;
    public int finalJudgement = 0;
    public int holyFire = 0;
    public GameObject deathScreen;
    public SpriteRenderer spriteRenderer;
    public Sprite lightGwain;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<playerMovement>().jump = 10 + finalJudgement*5;
        this.GetComponent<playerMovement>().speed = 2 + atonement*5;
        ZapCooldown = 0.5f - fearOfGod/10;
        Health = 10 + divineProtection;
        lightDamage = 10 + holyFire * 10;
        darkDamage = 10 + corruption * 10;
        if(polarity >= 1)
        {
            spriteRenderer.sprite = lightGwain; 
        }
        lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        firePos.transform.rotation = Quaternion.Euler(0f, 0f, lookAngle -90f);

        if(Time.time > ZapStart + ZapCooldown)
        {
            
            if(Input.GetMouseButtonDown(0))
            {
                lightZap();
                ZapStart = Time.time;
            }
            else if(Input.GetMouseButtonDown(1))
            {
                darkZap();
                ZapStart = Time.time;
            }
        }
    }

    private void lightZap()
    {
        GameObject firedLightZap = Instantiate(lightProjectile, firePos.position, firePos.rotation * Quaternion.Euler(0, 0, 90));
        firedLightZap.GetComponent<Rigidbody2D>().velocity = firePos.up * 100f;
    }
    private void darkZap()
    {
        GameObject firedDarkZap = Instantiate(darkProjectile, firePos.position, firePos.rotation * Quaternion.Euler(0, 0, 90));
        firedDarkZap.GetComponent<Rigidbody2D>().velocity = firePos.up * 100f;
    }
    void hit()
    {
        Health -= 1;
        this.GetComponent<healthScript>().health -= 1;
        if (Health <= 0)
        {
            Time.timeScale = 0f;
            deathScreen.SetActive(true);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("car");
        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "devil" || collision.gameObject.tag == "angel")
        {
            Debug.Log("far");
            //If the GameObject has the same tag as specified, output this message in the console
            hit();
        }
    }
    
}
