using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abilityMenu : MonoBehaviour
{
    public List<GameObject> abilities;
    public GameObject player;
    public GameObject canvas;
    public GameObject camera;
    public GameObject gameManager;
    //public Transform panel;
    //public Transform bigthing;
    public bool newAbility = false;
    public GameObject selectionMenu;
    private GameObject kennedyReeves;
    private GameObject button1;
    private GameObject button2;
    private GameObject button3;
    public int num1;
    public int num2;
    public int num3;
    private float ZapStart = 0f;
    private float ZapCooldown = 2f;


    void Start()
    {
        selectionMenu.SetActive(false);
    }
    // Update is called once per frame
    /*void Update()
    {
        if (gameManager.GetComponent<gameManager>().waveCleared == true && Time.time > ZapStart + ZapCooldown);
        {
            Debug.Log(Time.time);
            ZapStart = Time.time;
            
            //gameManager.GetComponent<gameManager>().waveCleared = false;
            Selection();
            
        }
    }*/

    public void Selection()
    {
        Debug.Log("pee");
        //GameObject Canvas = Instantiate(canvas, new Vector2(0,0), Quaternion.identity);
        //kennedyReeves = Instantiate(selectionMenu, new Vector3(0,0,0), Quaternion.identity);
        //kennedyReeves.transform.parent = bigthing;
        //this.panel = kennedyReeves.transform;
        int randomNumber = Random.Range(0, abilities.Count);
        button1 = Instantiate(abilities[randomNumber], new Vector3(220,262,0), Quaternion.identity);
        button1.transform.parent = canvas.transform;
        num1 = randomNumber;
        while (randomNumber == num1)
        {
            randomNumber = Random.Range(0, abilities.Count);
        }
        button2 = Instantiate(abilities[randomNumber], new Vector3(575,262,0), Quaternion.identity);
        button2.transform.parent = canvas.transform; 
        num2 = randomNumber;
        while (randomNumber == num1 || randomNumber == num2)
        {
            randomNumber = Random.Range(0, abilities.Count);
        }
        button3 = Instantiate(abilities[randomNumber], new Vector3(930,262,0), Quaternion.identity);
        button3.transform.parent = canvas.transform; 

        selectionMenu.SetActive(true);
        //player.GetComponent<fireProjectile>().enabled = false;
        Time.timeScale = 0f;
        //GameObject newPlayer = Instantiate(player, new Vector2(-16, -2), Quaternion.identity);
        //camera.GetComponent<CameraFollow>().Player = newPlayer.transform;
        //Destroy(player);
        
    }

    void Resume()
    {
        //Destroy(kennedyReeves);
        Destroy(GameObject.Find ("atonement(Clone)"));
        Destroy(GameObject.Find ("blight(Clone)"));
        Destroy(GameObject.Find ("corruption(Clone)"));
        Destroy(GameObject.Find ("holyFire(Clone)"));
        Destroy(GameObject.Find ("fearOfGod(Clone)"));
        Destroy(GameObject.Find ("oneMoreInTheChamber(Clone)"));
        Destroy(GameObject.Find ("finalJudgement(Clone)"));
        Destroy(GameObject.Find ("divineProtection(Clone)"));
     
        Time.timeScale = 1f;
        //gameManager.GetComponent<gameManager>().newScene();
        selectionMenu.SetActive(false);
        //player.GetComponent<fireProjectile>().enabled = true;
        
    }

    public void blightChoice()
    {   
        Debug.Log("Help");
        player.GetComponent<fireProjectile>().blight++;
        player.GetComponent<fireProjectile>().polarity -= 0.2f;
        Resume();
    }
    public void corruptionChoice()
    {
        Debug.Log("Help");
        player.GetComponent<fireProjectile>().corruption++;
        player.GetComponent<fireProjectile>().polarity -= 0.2f;
        Resume();
    }
    public void atonementChoice()
    {
        player.GetComponent<fireProjectile>().atonement++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }
    public void fearOfGodChoice()
    {
        player.GetComponent<fireProjectile>().fearOfGod++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }
    public void finalJudgementChoice()
    {
        player.GetComponent<fireProjectile>().finalJudgement++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }
    public void oneMoreInTheChamberChoice()
    {
        player.GetComponent<fireProjectile>().oneMoreInTheChamber++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }
    public void holyFireChoice()
    {
        player.GetComponent<fireProjectile>().holyFire++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }
    public void divineProtectionChoice()
    {
        player.GetComponent<fireProjectile>().divineProtection++;
        player.GetComponent<fireProjectile>().polarity += 0.2f;
        Resume();
    }

}
