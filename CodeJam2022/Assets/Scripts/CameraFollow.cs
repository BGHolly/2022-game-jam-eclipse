using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform Player;
    public float dampTime = 0f;
    private Vector3 cameraPos;
    private Vector3 velocity = Vector3.zero;
    float posX;
    float posY;

    void Update() {
        if (Player.position.x < -8) {
            posX = -8;
        }
        else if (Player.position.x > 5) {
            posX = 5;
        }
        else {
            posX = Player.position.x;
        }
        if (Player.position.y < 1) {
            posY = 1;
        }
        else {
            posY = Player.position.y;
        }

        cameraPos = new Vector3(posX, posY, -10f);
        transform.position = Vector3.SmoothDamp(gameObject.transform.position, cameraPos, ref velocity, dampTime);
    }
}
