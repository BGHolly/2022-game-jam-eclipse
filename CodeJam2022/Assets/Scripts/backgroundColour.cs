using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundColour : MonoBehaviour
{
    public Camera cammy;
    Color baseColour = new Color(0.52F, 0.81F, 0.92F, 1F);
    // Start is called before the first frame update
    void Start()
    {
        
        cammy.backgroundColor = baseColour;
    }

    // Update is called once per frame
    void changeBackground(float delta) {
        if (delta > 0) {
            cammy.backgroundColor = Color.Lerp(Color.white, baseColour, delta);
        }
        else {
            cammy.backgroundColor = Color.Lerp(Color.black, baseColour, delta);
        }
    }
}
