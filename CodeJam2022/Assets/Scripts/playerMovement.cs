using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    //public CharacterController2D controller;
    public float speed;
    public int freeze = 1;
    public float jump;
    private Rigidbody2D rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Update()
    {
        
        var horizontalMove = Input.GetAxisRaw("Horizontal") * speed * freeze;
        /*if (horizontalMove > 10) {
            horizontalMove = 10;
        }
        else if (horizontalMove < -20) {
            horizontalMove = -20;
        }*/
        transform.position +=  new Vector3(horizontalMove, 0, 0) * Time.fixedDeltaTime * speed;

        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidBody.velocity.y) < 0.001f)
        {
            rigidBody.AddForce(new Vector2(0, jump), ForceMode2D.Impulse);
        }

        Vector3 characterScale = transform.localScale;
        if (Input.GetAxis("Horizontal") < 0)
        {
            characterScale.x = -1;
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            characterScale.x = 1;
        }
        transform.localScale = characterScale;
        
    }

    public void ResetPos()
    {
        transform.position = new Vector2(-16, -2);
    }

    // Update is called once per frame
}